Launched by award-winning architect, builder and interior designer, Phil Kean, AIA, Phil Kean Kitchens fuses hand-selected elements to create unique, one-of-a kind kitchen and bath spaces. We serve as a single source for local homeowners, contractors, architects and designers.

Address: 952 W. Fairbanks Ave, Winter Park, FL 32789

Phone: 407-622-1636